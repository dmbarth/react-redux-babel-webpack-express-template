import * as React from 'react'
import { connect } from 'react-redux'

class App extends React.Component<any, any>{
	render(){       
        return (
            <div id='app'>{this.props.children}</div>
        )
	}
}

export default connect(state => {
    return {
        path: state.routing.path
    }
})(App)