var config = require('./config');

module.exports = {
    production: true,
    server: {
        host: config.server.host.local,
        port: config.server.port.production
    },
    client: {
        production: true,
        api: config.api.obj(
            config.api.host.domain,
            config.api.port.production
        ),
        auth: config.auth
    }
}