var express = require('express'),
    browserSync = require('browser-sync'),
    config = require('getconfig'),
    app = express();

// configure server
var host = config.server.host,
    port = config.server.port,
    proxy = host + ":" + port;

// configure browser-sync
function listening() {
  if (config.development){
      browserSync({
      proxy: proxy,
      files: ['app/**/*.{js,css}'],
      open: false,
      reloadOnRestart: true
    });
  } else {
    console.log('Listening on: ' + proxy);
  }  
}

// middleware for setting cookie config variables
var configMiddleware = function (req, res, next) {
  res.cookie('config', JSON.stringify(config.client));
  next();
};

app.use(express.static(__dirname + '/app'));

app.get('*', configMiddleware, function (req, res) {
    res.sendFile(__dirname + '/app/app.html');
});

// listen for changes with nodemon and browser-sync
app.listen(port, listening);