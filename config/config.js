module.exports = {
    api: {
        host: {
            local: 'localhost',
            domain: 'api.davidbarth.website',
        },
        port: {
            development: 8001,
            production: 81
        },
        obj: function(host, port){
            return { host, port,
                uri: [ 'http://', host, ':', port, '/api/' ].join('')
            };
        }
    },
    server: {
        host: {
            local: 'localhost'
        },
        port: {
            development: 3005,
            production: 3000
        }
    }
}