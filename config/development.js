var config = require('./config');

module.exports = {
    development: true,
    server: {
        host: config.server.host.local,
        port: config.server.port.development
    },
    client: {
        development: true,
        api: config.api.obj(
            config.api.host.domain,
            config.api.port.staging
        ),
        auth: config.auth
    }
}