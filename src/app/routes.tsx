import * as React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import store from './store'

var Config = require('clientconfig');

// VIEWS
import App from './app'

const history = syncHistoryWithStore(browserHistory, store)

export default class Routes extends React.Component<any, any> {
    render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <Route path='/' component={App} >
                    </Route>
                </Router>
            </Provider>
        )
    }
}