var webpack = require('webpack');

module.exports = {

    debug: true,
    devtool: 'source-map',

    entry: {
        app: './src/app',
        vendors: [
            'babel-polyfill',
            'react',
            'lodash'
        ]
    },

    output: {
        path: './app',
        filename: 'app.js'
    },

    module: {
        loaders: [{
            test: /\.ts(x?)$/,
            exclude: /node_modules/,
            loader: 'babel-loader?presets[]=es2015&presets[]=react!ts-loader'
        }, {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['es2015', 'react']
            }
        }]
    },

    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.ts', '.tsx']
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
        // new webpack.optimize.UglifyJsPlugin({
        //   compress: { warnings: false }
        // }),
    ]
}
