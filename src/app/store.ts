import { createStore, applyMiddleware } from 'redux'
import Thunk from 'redux-thunk'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'

import Reducers from './reducers'
import Logger from './logger'

var Config = require('clientconfig');

const middleware = [
  Thunk, 
  routerMiddleware(browserHistory)
]

if(Config.development) middleware.push(Logger)

export default createStore(
  Reducers,
  applyMiddleware(...middleware)
)